import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalVariables } from "../global/global";
import { HomePage } from "../home/home";
import { ProfilePage } from "../profile/profile";
import { SettingsPage } from "../settings/settings";
import { ContactPage } from "../contact/contact";
import { LegalPage } from "../legal/legal";

@IonicPage()
@Component({
  selector: 'page-favorites[class="favorites-page-container"]',
  templateUrl: 'favorites.html',
})
export class FavoritesPage {

  constructor(public navCtrl: NavController, public global: GlobalVariables, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritesPage');
  }

  currentUserImage;
  ionViewWillEnter() {
    const thisPage = this;
    const userEmail = this.global.userEmail;
    fetch("https://jeantinland.com/projets/api_senses/user/get_user_image.php?email=" + userEmail)
      .then(res => {
        return res.json();
      })
      .then(data => {
        if (data.image_url == null) {
          thisPage.currentUserImage = 'https://jeantinland.com/projets/api_senses/images/users/default_avatar.png';
        } else {
          thisPage.currentUserImage = data.image_url;
        }
      });
  }

  goToHomePage() {
    this.navCtrl.push(HomePage, {}, { animate: false })
      .then(removePage => {
        let page = document.getElementsByClassName('home-page-container');
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function(node) {
            node.parentNode.removeChild(node);
          });
        }
      });
  }

  goToProfilePage() {
    this.navCtrl.push(ProfilePage, {}, { animate: false })
      .then(removePage => {
        let page = document.getElementsByClassName('profile-page-container');
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function(node) {
            node.parentNode.removeChild(node);
          });
        }
      });
  }

  goToSettingsPage() {
    this.navCtrl.push(SettingsPage, {}, { animate: false })
      .then(removePage => {
        let page = document.getElementsByClassName('settings-page-container');
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function(node) {
            node.parentNode.removeChild(node);
          });
        }
      });
  }

  goToContactPage() {
    this.navCtrl.push(ContactPage, {}, { animate: false })
      .then(removePage => {
        let page = document.getElementsByClassName('contact-page-container');
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function(node) {
            node.parentNode.removeChild(node);
          });
        }
      });
  }

  goToLegalPage() {
    this.navCtrl.push(LegalPage, {}, { animate: false })
      .then(removePage => {
        let page = document.getElementsByClassName('legal-page-container');
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function(node) {
            node.parentNode.removeChild(node);
          });
        }
      });
  }
}
