import { Component } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { DomSanitizer } from '@angular/platform-browser'
import { Observable } from 'rxjs/Observable'
import { NavController } from 'ionic-angular'
import { GlobalVariables } from '../global/global'
import { ProfilePage } from '../profile/profile'
import { SettingsPage } from '../settings/settings'
import { ContactPage } from '../contact/contact'
import { FavoritesPage } from '../favorites/favorites'
import { LegalPage } from '../legal/legal'

@Component({
  selector: 'page-home[class="home-page-container"]',
  templateUrl: 'home.html'
})
export class HomePage {
  constructor(
    public navCtrl: NavController,
    public global: GlobalVariables,
    public httpClient: HttpClient,
    private sanitizer: DomSanitizer
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage')
    this.getTripsDatas()
  }

  currentUserImage
  ionViewWillEnter() {
    const thisPage = this
    const userEmail = this.global.userEmail
    fetch(
      'https://jeantinland.com/projets/api_senses/user/get_user_image.php?email=' +
        userEmail
    )
      .then(function(res) {
        return res.json()
      })
      .then(function(data) {
        if (data.image_url == null) {
          thisPage.currentUserImage =
            'https://jeantinland.com/projets/api_senses/images/users/default_avatar.png'
        } else {
          thisPage.currentUserImage = data.image_url
        }
      })
  }

  dataNewTrips: Array<any>
  dataPopularTrips: Array<any>
  dataBgNewTripsImage: any
  dataBgPopularTripsImage: any
  newTripsDatas: Observable<any>
  popTripsDatas: Observable<any>
  getTripsDatas() {
    return new Promise(resolve => {
      this.newTripsDatas = this.httpClient.get(
        'https://jeantinland.com/projets/api_senses/trip/read_cat.php?cat=new'
      )
      this.newTripsDatas.subscribe(data => {
        this.dataNewTrips = data.records
      })
      this.popTripsDatas = this.httpClient.get(
        'https://jeantinland.com/projets/api_senses/trip/read_cat.php?cat=pop'
      )
      this.popTripsDatas.subscribe(data => {
        this.dataPopularTrips = data.records
      })
    })
  }

  sanitizeImg(imgurl) {
    let sanitizedImg = this.sanitizer.bypassSecurityTrustStyle(
      'url(' + imgurl + ')'
    )
    return sanitizedImg
  }

  displayNewTrips() {
    let newTripsFilterbtn = document.querySelector('#new-trips-filter-btn')
    let newTripsContainer = document.querySelector('.new-trips')
    let tripsContainers = document.getElementsByClassName('trip-container')
    let filterBtns = document.getElementsByClassName('trip-filter-btn')
    for (let i = 0; i < tripsContainers.length; i++) {
      if (tripsContainers[i].classList.contains('visible')) {
        tripsContainers[i].classList.remove('visible')
      }
    }
    for (let i = 0; i < filterBtns.length; i++) {
      if (filterBtns[i].classList.contains('active')) {
        filterBtns[i].classList.remove('active')
      }
    }
    if (!newTripsFilterbtn.classList.contains('active')) {
      newTripsFilterbtn.classList.add('active')
      newTripsContainer.classList.add('visible')
    }
  }

  displayPopularTrips() {
    let popularTripsFilterbtn = document.querySelector(
      '#popular-trips-filter-btn'
    )
    let popularTripsContainer = document.querySelector('.popular-trips')
    let tripsContainers = document.getElementsByClassName('trip-container')
    let filterBtns = document.getElementsByClassName('trip-filter-btn')
    for (let i = 0; i < tripsContainers.length; i++) {
      if (tripsContainers[i].classList.contains('visible')) {
        tripsContainers[i].classList.remove('visible')
      }
    }
    for (let i = 0; i < filterBtns.length; i++) {
      if (filterBtns[i].classList.contains('active')) {
        filterBtns[i].classList.remove('active')
      }
    }
    if (!popularTripsFilterbtn.classList.contains('active')) {
      popularTripsFilterbtn.classList.add('active')
      popularTripsContainer.classList.add('visible')
    }
  }

  displayAmbiancesContainer() {
    let ambianceFilterbtn = document.querySelector('#ambiance-filter-btn')
    let ambianceContainer = document.querySelector('.ambiance-container')
    let tripsContainers = document.getElementsByClassName('trip-container')
    let filterBtns = document.getElementsByClassName('trip-filter-btn')
    for (let i = 0; i < tripsContainers.length; i++) {
      if (tripsContainers[i].classList.contains('visible')) {
        tripsContainers[i].classList.remove('visible')
      }
    }
    for (let i = 0; i < filterBtns.length; i++) {
      if (filterBtns[i].classList.contains('active')) {
        filterBtns[i].classList.remove('active')
      }
    }
    if (!ambianceFilterbtn.classList.contains('active')) {
      ambianceFilterbtn.classList.add('active')
      ambianceContainer.classList.add('visible')
    }
  }

  displayAllTrips() {
    let allTripsFilterbtn = document.querySelector('#all-filter-btn')
    let allTripsContainer = document.querySelector('.all-trips-container')
    let tripsContainers = document.getElementsByClassName('trip-container')
    let filterBtns = document.getElementsByClassName('trip-filter-btn')
    for (let i = 0; i < tripsContainers.length; i++) {
      if (tripsContainers[i].classList.contains('visible')) {
        tripsContainers[i].classList.remove('visible')
      }
    }
    for (let i = 0; i < filterBtns.length; i++) {
      if (filterBtns[i].classList.contains('active')) {
        filterBtns[i].classList.remove('active')
      }
    }
    if (!allTripsFilterbtn.classList.contains('active')) {
      allTripsFilterbtn.classList.add('active')
      allTripsContainer.classList.add('visible')
    }
  }

  displayNewTrip(tripId) {
    let newTripVideoContainer = document.getElementsByClassName(
      'video-new-trip-' + tripId
    )[0]
    let allTripVideoCOntainer = document.getElementsByClassName(
      'video-container'
    )
    for (let i = 0; i < allTripVideoCOntainer.length; i++) {
      if (allTripVideoCOntainer[i].classList.contains('visible'))
        allTripVideoCOntainer[i].classList.remove('visible')
    }
    newTripVideoContainer.classList.add('visible')
  }

  displayPopularTrip(tripId) {
    let newTripVideoContainer = document.getElementsByClassName(
      'video-popuplar-trip-' + tripId
    )[0]
    let allTripVideoCOntainer = document.getElementsByClassName(
      'video-container'
    )
    for (let i = 0; i < allTripVideoCOntainer.length; i++) {
      if (allTripVideoCOntainer[i].classList.contains('visible'))
        allTripVideoCOntainer[i].classList.remove('visible')
    }
    newTripVideoContainer.classList.add('visible')
  }

  closeTripVideoWrapper() {
    let allTripVideoContainer = document.getElementsByClassName(
      'video-container'
    )
    let allTripVideoIframe = document.getElementsByClassName('video-iframe')
    for (let i = 0; i < allTripVideoContainer.length; i++) {
      if (allTripVideoContainer[i].classList.contains('visible'))
        allTripVideoContainer[i].classList.remove('visible')
    }
    for (let i = 0; i < allTripVideoIframe.length; i++) {
      allTripVideoIframe[i].src = allTripVideoIframe[i].src
    }
  }

  goToProfilePage() {
    this.navCtrl.push(ProfilePage, {}, { animate: false }).then(removePage => {
      let page = document.getElementsByClassName('profile-page-container')
      while (page.length > 1) {
        Array.prototype.forEach.call(page, function(node) {
          node.parentNode.removeChild(node)
        })
      }
    })
  }

  goToFavoritesPage() {
    this.navCtrl
      .push(FavoritesPage, {}, { animate: false })
      .then(removePage => {
        let page = document.getElementsByClassName('favorites-page-container')
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function(node) {
            node.parentNode.removeChild(node)
          })
        }
      })
  }

  goToSettingsPage() {
    this.navCtrl.push(SettingsPage, {}, { animate: false }).then(removePage => {
      let page = document.getElementsByClassName('settings-page-container')
      while (page.length > 1) {
        Array.prototype.forEach.call(page, function(node) {
          node.parentNode.removeChild(node)
        })
      }
    })
  }

  goToContactPage() {
    this.navCtrl.push(ContactPage, {}, { animate: false }).then(removePage => {
      let page = document.getElementsByClassName('contact-page-container')
      while (page.length > 1) {
        Array.prototype.forEach.call(page, function(node) {
          node.parentNode.removeChild(node)
        })
      }
    })
  }

  goToLegalPage() {
    this.navCtrl.push(LegalPage, {}, { animate: false }).then(removePage => {
      let page = document.getElementsByClassName('legal-page-container')
      while (page.length > 1) {
        Array.prototype.forEach.call(page, function(node) {
          node.parentNode.removeChild(node)
        })
      }
    })
  }
}
