import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalVariables } from "../global/global";
import { HomePage } from "../home/home";
import { ProfilePage } from "../profile/profile";
import { SettingsPage } from "../settings/settings";
import { ContactPage } from "../contact/contact";
import { FavoritesPage } from "../favorites/favorites";

@IonicPage()
@Component({
  selector: 'page-legal[class="legal-page-container"]',
  templateUrl: 'legal.html',
})
export class LegalPage {

  constructor(public navCtrl: NavController, public global: GlobalVariables, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LegalPage');
  }

  currentUserImage;
  ionViewWillEnter() {
    const thisPage = this;
    const userEmail = this.global.userEmail;
    fetch("https://jeantinland.com/projets/api_senses/user/get_user_image.php?email=" + userEmail)
      .then(res => {
        return res.json();
      })
      .then(data => {
        if (data.image_url == null) {
          thisPage.currentUserImage = 'https://jeantinland.com/projets/api_senses/images/users/default_avatar.png';
        } else {
          thisPage.currentUserImage = data.image_url;
        }
      });
  }



  pageOne() {
    const legalPageOneContainer = document.querySelector('.page-one');
    const legalPageTwoContainer = document.querySelector('.page-two');
    const legalPageThreeContainer = document.querySelector('.page-three');
    const legalPageFirstBtn = document.querySelector('.page-one-button');
    const legalPageSecondBtn = document.querySelector('.page-two-button');
    const legalPageThirdBtn = document.querySelector('.page-three-button');
    if (legalPageTwoContainer.classList.contains('visible')) {
      legalPageTwoContainer.classList.remove('visible');
      legalPageSecondBtn.classList.remove('active');
    }
    if (legalPageThreeContainer.classList.contains('visible')) {
      legalPageThreeContainer.classList.remove('visible');
      legalPageThirdBtn.classList.remove('active');
    }
    legalPageOneContainer.classList.add('visible');
    legalPageFirstBtn.classList.add('active');
  }

  pageTwo() {
    const legalPageOneContainer = document.querySelector('.page-one');
    const legalPageTwoContainer = document.querySelector('.page-two');
    const legalPageThreeContainer = document.querySelector('.page-three');
    const legalPageFirstBtn = document.querySelector('.page-one-button');
    const legalPageSecondBtn = document.querySelector('.page-two-button');
    const legalPageThirdBtn = document.querySelector('.page-three-button');
    if (legalPageOneContainer.classList.contains('visible')) {
      legalPageOneContainer.classList.remove('visible');
      legalPageFirstBtn.classList.remove('active');
    }
    if (legalPageThreeContainer.classList.contains('visible')) {
      legalPageThreeContainer.classList.remove('visible');
      legalPageThirdBtn.classList.remove('active');
    }
    legalPageTwoContainer.classList.add('visible');
    legalPageSecondBtn.classList.add('active');
  }

  pageThree() {
    const legalPageOneContainer = document.querySelector('.page-one');
    const legalPageTwoContainer = document.querySelector('.page-two');
    const legalPageThreeContainer = document.querySelector('.page-three');
    const legalPageFirstBtn = document.querySelector('.page-one-button');
    const legalPageSecondBtn = document.querySelector('.page-two-button');
    const legalPageThirdBtn = document.querySelector('.page-three-button');
    if (legalPageOneContainer.classList.contains('visible')) {
      legalPageOneContainer.classList.remove('visible');
      legalPageFirstBtn.classList.remove('active');
    }
    if (legalPageTwoContainer.classList.contains('visible')) {
      legalPageTwoContainer.classList.remove('visible');
      legalPageSecondBtn.classList.remove('active');
    }
    legalPageThreeContainer.classList.add('visible');
    legalPageThirdBtn.classList.add('active');
  }

  goToHomePage() {
    this.navCtrl.push(HomePage, {}, { animate: false })
      .then(removePage => {
        let page = document.getElementsByClassName('home-page-container');
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function(node) {
            node.parentNode.removeChild(node);
          });
        }
      });
  }

  goToProfilePage() {
    this.navCtrl.push(ProfilePage, {}, { animate: false })
      .then(removePage => {
        let page = document.getElementsByClassName('profile-page-container');
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function(node) {
            node.parentNode.removeChild(node);
          });
        }
      });
  }

  goToFavoritesPage() {
    this.navCtrl.push(FavoritesPage, {}, { animate: false })
      .then(removePage => {
        let page = document.getElementsByClassName('favorites-page-container');
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function(node) {
            node.parentNode.removeChild(node);
          });
        }
      });
  }

  goToSettingsPage() {
    this.navCtrl.push(SettingsPage, {}, { animate: false })
      .then(removePage => {
        let page = document.getElementsByClassName('settings-page-container');
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function(node) {
            node.parentNode.removeChild(node);
          });
        }
      });
  }

  goToContactPage() {
    this.navCtrl.push(ContactPage, {}, { animate: false })
      .then(removePage => {
        let page = document.getElementsByClassName('contact-page-container');
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function(node) {
            node.parentNode.removeChild(node);
          });
        }
      });
  }
}
