import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TutorialPage } from "../tutorial/tutorial";
import { GlobalVariables } from "../global/global";

@IonicPage()
@Component({
  selector: 'page-welcome[class="welcome-page-container"]',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public global: GlobalVariables) {

  }

  // Test witch container position (left & right handed mode)
  switchSides() {
    const switchLeftBtn = document.querySelector('.switch-left');
    const switchRightBtn = document.querySelector('.switch-right');
    const leftContentWrapper = document.querySelector('.content-wrapper__left');
    const rightContentWrapper = document.querySelector('.content-wrapper__right');
    if (switchLeftBtn.classList.contains("active")) {
      switchLeftBtn.classList.remove("active");
      switchRightBtn.classList.add("active");
    } else {
      switchLeftBtn.classList.add("active");
      switchRightBtn.classList.remove("active");
    }
    if (leftContentWrapper.classList.contains("switched") || rightContentWrapper.classList.contains("switched")) {
      leftContentWrapper.classList.remove("switched");
      rightContentWrapper.classList.remove("switched");
    } else {
      leftContentWrapper.classList.add("switched");
      rightContentWrapper.classList.add("switched");
    }
  }

  showLoginForm() {
    const welcomeWrapper = document.querySelector('.welcome-wrapper');
    const registerWrapper = document.querySelector('.register-wrapper');
    const loginWrapper = document.querySelector('.login-wrapper');
    const flagContainer = document.querySelector('.flag-container');
    if (!registerWrapper.classList.contains("hidden")) {
      registerWrapper.classList.add('hidden');
      loginWrapper.classList.remove('hidden');
    }
    if (!welcomeWrapper.classList.contains("hidden")) {
      welcomeWrapper.classList.add('hidden');
      loginWrapper.classList.remove('hidden');
    }
    if (flagContainer.classList.contains("hidden")) {
      flagContainer.classList.remove('hidden');
    }
  }

  showRegisterForm() {
    const welcomeWrapper = document.querySelector('.welcome-wrapper');
    const registerWrapper = document.querySelector('.register-wrapper');
    const loginWrapper = document.querySelector('.login-wrapper');
    const flagContainer = document.querySelector('.flag-container');
    if (!loginWrapper.classList.contains("hidden")) {
      loginWrapper.classList.add('hidden');
      registerWrapper.classList.remove('hidden');
    }
    if (!welcomeWrapper.classList.contains("hidden")) {
      welcomeWrapper.classList.add('hidden');
      registerWrapper.classList.remove('hidden');
    }
    if (flagContainer.classList.contains("hidden")) {
      flagContainer.classList.remove('hidden');
    }
  }

  registerData = {};
  userRegistration() {
    const thisPage = this;
    const data = JSON.stringify(this.registerData);
    const jsonRegisterData = JSON.parse(data);
    const errorMessage = document.querySelector('.error-register-message');
    const createdUserEmail = jsonRegisterData.email;
    const userRegistrationUrl = this.global.userRegistrationUrl;
    if (data == "{}" || jsonRegisterData.first == '' || jsonRegisterData.last == '' || jsonRegisterData.email == '' || jsonRegisterData.password == '' || jsonRegisterData.first == undefined || jsonRegisterData.last == undefined || jsonRegisterData.email == undefined || jsonRegisterData.password == undefined) {
      errorMessage.classList.add('visible');
    } else {
      if (errorMessage.classList.contains('visible')) errorMessage.classList.remove('visible');
      fetch(userRegistrationUrl,
        {
          method: "POST",
          body: data
        })
        .then(function(res){
          return res.json();
        })
        .then(function(){
          thisPage.global.loginState = true;
          thisPage.global.userEmail = createdUserEmail;
          thisPage.navCtrl.push(TutorialPage)
            .then(removePage => {
              let page = document.getElementsByClassName('tutorial-page-container');
              while (page.length > 1) {
                Array.prototype.forEach.call(page, function (node) {
                  node.parentNode.removeChild(node);
                });
              }
            });
        });
    }
  }

  loginData = {};
  userLogin() {
    const thisPage = this;
    const data = JSON.stringify(this.loginData);
    const userLoginUrl = this.global.userLoginUrl;
    fetch(userLoginUrl,
      {
        method: "POST",
        body: data
      })
      .then(function(res){
        return res.json();
      })
      .then(function(data){
        console.log(JSON.stringify( data ));
        const errorMessage = document.querySelector('.error-login-message');
        const userEmail = data.email;
        if (data.email == null || data.password == null || data.email == '' || data.password == '') {
          errorMessage.classList.add('visible');
        } else {
          if (errorMessage.classList.contains('visible')) errorMessage.classList.remove('visible');
          thisPage.global.loginState = true;
          thisPage.global.userEmail = userEmail;
          thisPage.navCtrl.push(TutorialPage)
            .then(removePage => {
              let page = document.getElementsByClassName('tutorial-page-container');
              while (page.length > 1) {
                Array.prototype.forEach.call(page, function (node) {
                  node.parentNode.removeChild(node);
                });
              }
            });
        }
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomePage');
  }

}
