import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from "../home/home";
import { GlobalVariables } from "../global/global";
import { WelcomePage } from "../welcome/welcome";
import { SettingsPage } from "../settings/settings";
import { ContactPage } from "../contact/contact";
import { FavoritesPage } from "../favorites/favorites";
import { LegalPage } from "../legal/legal";

@IonicPage()
@Component({
  selector: 'page-profile[class="profile-page-container"]',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  constructor(public navCtrl: NavController, public global: GlobalVariables, public navParams: NavParams) {
  }

  currentUserFirst;
  currentUserLast;
  currentUserEmail;
  currentUserPassword;
  currentUserImage;
  userData = {};
  ionViewDidLoad() {
    const thisPage = this;
    const userEmail = this.global.userEmail;
    fetch("https://jeantinland.com/projets/api_senses/user/get_user_image.php?email=" + userEmail)
      .then(function (res) {
        return res.json();
      })
      .then(function (data) {
        thisPage.currentUserFirst = data.first;
        thisPage.currentUserLast = data.last;
        thisPage.currentUserEmail = data.email;
        thisPage.currentUserPassword = data.password;
        thisPage.userData = {
          "first": data.first,
          "last": data.last,
          "email": data.email,
          "password": data.password
        };
        if (data.image_url == null) {
          thisPage.currentUserImage = 'https://jeantinland.com/projets/api_senses/images/users/default_avatar.png';
        } else {
          thisPage.currentUserImage = data.image_url;
        }
      });
  }

  userSaveData() {
    const thisPage = this;
    const data = JSON.stringify(this.userData);
    const jsonUserSaveData = JSON.parse(data);
    console.log(jsonUserSaveData);
    const errorMessage = document.querySelector('.error-update-message');
    if (jsonUserSaveData.first == '' || jsonUserSaveData.last == '' || jsonUserSaveData.email == '' || jsonUserSaveData.password == '' || jsonUserSaveData.first == undefined || jsonUserSaveData.last == undefined || jsonUserSaveData.email == undefined || jsonUserSaveData.password == undefined) {
      errorMessage.classList.add('visible');
    } else {
      if (errorMessage.classList.contains('visible')) errorMessage.classList.remove('visible');
      this.global.loaderContainer.classList.add('visible');
      fetch("https://jeantinland.com/projets/api_senses/user/update.php",
        {
          method: "POST",
          body: data
        })
        .then(function(res){
          return res.json();
        })
        .then(function(){
          console.log(JSON.stringify( data ));
          setTimeout(function () {
            thisPage.global.loaderContainer.classList.remove('visible');
          }, 1000);
        });
    }
  }

  disconnectUser() {
    this.navCtrl.push(WelcomePage)
      .then(removePage => {
        let page = document.getElementsByClassName('welcome-page-container');
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function (node) {
            node.parentNode.removeChild(node);
          });
        }
      });
  }

  goToHomePage() {
    this.navCtrl.push(HomePage, {}, { animate: false })
      .then(removePage => {
        let page = document.getElementsByClassName('home-page-container');
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function(node) {
            node.parentNode.removeChild(node);
          });
        }
      });
  }

  goToFavoritesPage() {
    this.navCtrl.push(FavoritesPage, {}, { animate: false })
      .then(removePage => {
        let page = document.getElementsByClassName('favorites-page-container');
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function(node) {
            node.parentNode.removeChild(node);
          });
        }
      });
  }

  goToSettingsPage() {
    this.navCtrl.push(SettingsPage, {}, { animate: false })
      .then(removePage => {
        let page = document.getElementsByClassName('settings-page-container');
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function(node) {
            node.parentNode.removeChild(node);
          });
        }
      });
  }

  goToContactPage() {
    this.navCtrl.push(ContactPage, {}, { animate: false })
      .then(removePage => {
        let page = document.getElementsByClassName('contact-page-container');
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function(node) {
            node.parentNode.removeChild(node);
          });
        }
      });
  }

  goToLegalPage() {
    this.navCtrl.push(LegalPage, {}, { animate: false })
      .then(removePage => {
        let page = document.getElementsByClassName('legal-page-container');
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function(node) {
            node.parentNode.removeChild(node);
          });
        }
      });
  }
}
