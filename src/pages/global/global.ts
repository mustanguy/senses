import { Injectable } from '@angular/core'

@Injectable()
export class GlobalVariables {
  public loginState: boolean = false
  public userEmail

  // Global api variables
  public userRegistrationUrl: string = 'https://jeantinland.com/projets/api_senses/user/create.php'
  public userLoginUrl: string = 'https://jeantinland.com/projets/api_senses/user/login.php'

  // Loader container selector variable
  public loaderContainer = document.querySelector('.loader')
}
