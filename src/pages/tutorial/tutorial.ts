import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HomePage} from "../home/home";

@IonicPage()
@Component({
  selector: 'page-tutorial[class="tutorial-page-container"]',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  stepOne() {
    const firstText = document.querySelector('.first-text');
    const firstPoint = document.querySelector('.first-point');
    const secondText = document.querySelector('.second-text');
    const secondPoint = document.querySelector('.second-point');
    const thirdText = document.querySelector('.third-text');
    const thirdPoint = document.querySelector('.third-point');
    const pageOneBtn = document.querySelector('.page-one');
    const pageTwoBtn = document.querySelector('.page-two');
    const pageThreeBtn = document.querySelector('.page-three');
    if (!secondText.classList.contains('hidden')) {
      secondText.classList.add('hidden');
      secondPoint.classList.remove('revealed');
      pageTwoBtn.classList.remove('active');
    }
    if (!thirdText.classList.contains('hidden')) {
      thirdText.classList.add('hidden');
      thirdPoint.classList.remove('revealed');
      pageThreeBtn.classList.remove('active');
    }
    firstText.classList.remove('hidden');
    firstPoint.classList.add('revealed');
    pageOneBtn.classList.add('active');
  }

  stepTwo() {
    const firstText = document.querySelector('.first-text');
    const firstPoint = document.querySelector('.first-point');
    const secondText = document.querySelector('.second-text');
    const secondPoint = document.querySelector('.second-point');
    const thirdText = document.querySelector('.third-text');
    const thirdPoint = document.querySelector('.third-point');
    const pageOneBtn = document.querySelector('.page-one');
    const pageTwoBtn = document.querySelector('.page-two');
    const pageThreeBtn = document.querySelector('.page-three');
    if (!firstText.classList.contains('hidden')) {
      firstText.classList.add('hidden');
      firstPoint.classList.remove('revealed');
      pageOneBtn.classList.remove('active');
    }
    if (!thirdText.classList.contains('hidden')) {
      thirdText.classList.add('hidden');
      thirdPoint.classList.remove('revealed');
      pageThreeBtn.classList.remove('active');
    }
    secondText.classList.remove('hidden');
    secondPoint.classList.add('revealed');
    pageTwoBtn.classList.add('active');
  }

  stepThree() {
    const firstText = document.querySelector('.first-text');
    const firstPoint = document.querySelector('.first-point');
    const secondText = document.querySelector('.second-text');
    const secondPoint = document.querySelector('.second-point');
    const thirdText = document.querySelector('.third-text');
    const thirdPoint = document.querySelector('.third-point');
    const pageOneBtn = document.querySelector('.page-one');
    const pageTwoBtn = document.querySelector('.page-two');
    const pageThreeBtn = document.querySelector('.page-three');
    if (!firstText.classList.contains('hidden')) {
      firstText.classList.add('hidden');
      firstPoint.classList.remove('revealed');
      pageOneBtn.classList.remove('active');
    }
    if (!secondText.classList.contains('hidden')) {
      secondText.classList.add('hidden');
      secondPoint.classList.remove('revealed');
      pageTwoBtn.classList.remove('active');
    }
    thirdText.classList.remove('hidden');
    thirdPoint.classList.add('revealed');
    pageThreeBtn.classList.add('active');
  }

  stepFour() {
    this.navCtrl.push(HomePage)
      .then(removePage => {
        let page = document.getElementsByClassName('home-page-container');
        while (page.length > 1) {
          Array.prototype.forEach.call(page, function (node) {
            node.parentNode.removeChild(node);
          });
        }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TutorialPage');
  }

}
