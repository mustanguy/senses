import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { GlobalVariables } from '../pages/global/global';
import { HomePage } from '../pages/home/home';
import { WelcomePage } from '../pages/welcome/welcome';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { ProfilePage } from '../pages/profile/profile';
import { SettingsPage } from "../pages/settings/settings";
import { ContactPage } from "../pages/contact/contact";
import { FavoritesPage } from "../pages/favorites/favorites";
import { LegalPage } from "../pages/legal/legal";


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    WelcomePage,
    TutorialPage,
    ProfilePage,
    SettingsPage,
    ContactPage,
    FavoritesPage,
    LegalPage,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    WelcomePage,
    TutorialPage,
    ProfilePage,
    SettingsPage,
    ContactPage,
    FavoritesPage,
    LegalPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ScreenOrientation,
    GlobalVariables,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]
})
export class AppModule {}
